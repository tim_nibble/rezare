import { createBrowserHistory } from 'history';
import { Route, Switch } from 'react-router-dom';
import { Router } from 'react-router';
import { QueryClient, QueryClientProvider } from 'react-query';

import Container from '@material-ui/core/Container';
import { ThemeProvider } from '@material-ui/core/styles';

import Login from './views/Login';
import Students from './views/Students';

import theme from './theme';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router history={createBrowserHistory()}>
        <ThemeProvider theme={theme}>
          <Container>
            <div className="App">
              <Switch>
                <Route path="/" component={Login} exact />
                <Route path="/students" component={Students} exact />
              </Switch>
            </div>
          </Container>
        </ThemeProvider>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
