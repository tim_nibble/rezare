import { createMuiTheme } from '@material-ui/core/styles';

const PRIMARY_COLOR = 'rgb(0, 102, 204)';
const SECONDARY_COLOR = '#4F4F4F';
const BACKGROUND_COLOR = 'rgb(217,217,217)';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: PRIMARY_COLOR,
    },
    secondary: {
      main: SECONDARY_COLOR,
    },
    background: {
      default: BACKGROUND_COLOR,
      paper: '#fff',
    },
  },
  typography: {
    fontFamily: 'Poppins, sans-serif',
    button: {
      fontFamily: 'Poppins, sans-serif',
      fontWeight: 600,
      textTransform: 'none',
    },
  },
  overrides: {
    MuiTableCell: {
      stickyHeader: {
        backgroundColor: PRIMARY_COLOR,
        color: '#fff',
      },
    },
    MuiTableSortLabel: {
      root: {
        '&:hover': {
          color: '#222 !important',
        },
      },
      active: {
        color: '#fff !important',
        '&:hover': {
          color: '#222 !important',
        },
      },
    },
  },
});

export default theme;
