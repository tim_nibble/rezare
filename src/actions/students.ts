import axios from 'axios';

import { ApiResponse, Student } from '../types';

export const updateStudent = async (student: Required<Student>) => {
  const { data } = await axios.put<ApiResponse<Student>>(
    'http://localhost:5000/api/students',
    student,
  );
  return data;
};

export const createStudent = async (student: Student) => {
  const { data } = await axios.post<ApiResponse<Student>>(
    'http://localhost:5000/api/students',
    student,
  );
  return data;
};
