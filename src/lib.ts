import { Student } from './types';

export const convertZodErrors = (data: Record<keyof Student, string[]>): Record<string, string> => {
  const errors: Record<string, string> = {};

  Object.entries(data).forEach(([key, value]) => {
    errors[key] = value[0];
  });

  return errors;

  // @ts-ignore
  // return Object.entries(data).map(([key, value]) => ({
  //   [key]: value[0],
  // }));
};
