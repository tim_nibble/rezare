import React from 'react';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

interface Props {
  description?: string;
  title: string;
}

const PageHeading: React.FC<Props> = ({ description, title }) => {
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="flex-start"
      alignItems="flex-start"
      py={3}
    >
      <Typography variant="h4">{title}</Typography>
      {description && <Typography variant="body2">{description}</Typography>}
    </Box>
  );
};

export default PageHeading;
