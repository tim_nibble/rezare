import React from 'react';

import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

interface Props extends Omit<CircularProgressProps, 'thickness'> {
  label: string;
}

const CenteredSpinner: React.FC<Props> = ({ label, ...props }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CircularProgress thickness={2} {...props} />
      {label && <Typography style={{ paddingTop: '10px' }}>{label}</Typography>}
    </div>
  );
};

export default CenteredSpinner;
