import React, { useEffect } from 'react';

import Box from '@material-ui/core/Box';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { styled, Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import CloseIcon from '@material-ui/icons/Close';

const CloseButton = styled(IconButton)(({ theme }) => ({
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(-1),
}));

interface Props extends DialogProps {
  onClose?: () => void;
  onOpen?: () => void;
}

const ResponsiveDialog: React.FC<Props> = ({
  children,
  maxWidth = 'sm',
  open,
  title,
  onClose,
  onOpen,
  PaperProps,
  ...props
}) => {
  const fullScreen = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down('xs')
  );

  useEffect(() => {
    if (open) {
      onOpen?.();
    }
    // Only want to run this effect when `open` changes
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <Dialog
      fullScreen={fullScreen}
      maxWidth={maxWidth}
      open={open}
      onClose={onClose}
      PaperProps={PaperProps}
      {...props}
    >
      {title && (
        <DialogTitle>
          <Box
            display='flex'
            alignItems='center'
            justifyContent='space-between'
          >
            {title}
            <CloseButton onClick={onClose}>
              <CloseIcon />
            </CloseButton>
          </Box>
        </DialogTitle>
      )}
      {children}
    </Dialog>
  );
};

export default ResponsiveDialog;
