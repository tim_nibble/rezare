export interface Student {
  _id: string | null;
  firstName: string;
  lastName: string;
  age: number;
  avgGrade: number;
}

export type ApiResponse<T> = {
  success: boolean;
  data: T;
};

export enum GradeResult {
  'Excellent' = 'excellent',
  'Good' = 'good',
  'Average' = 'average',
  'Bad' = 'bad',
  'Failing' = 'failing',
}

export const getGradeInfo = (avgGrade: number) => {
  if (avgGrade >= 90) {
    return { color: '#57e32c', label: GradeResult.Excellent };
  } else if (avgGrade >= 80) {
    return { color: '#b7dd29', label: GradeResult.Good };
  } else if (avgGrade >= 70) {
    return { color: '#ffe234', label: GradeResult.Average };
  } else if (avgGrade >= 60) {
    return { color: '#ffa534', label: GradeResult.Bad };
  } else {
    return { color: '#ff4545', label: GradeResult.Failing };
  }
};
