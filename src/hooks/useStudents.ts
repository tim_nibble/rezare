import axios from 'axios';
import { useQuery } from 'react-query';

import { ApiResponse, Student } from '../types';

export default function useStudents() {
  const hook = useQuery('students', async () => {
    const { data } = await axios.get<ApiResponse<Student[]>>('http://localhost:5000/api/students');
    return data.data;
  });

  // Always return an array so that we dont have to deal with `| undefined`
  return {
    ...hook,
    data: hook.data || [],
  };
}
