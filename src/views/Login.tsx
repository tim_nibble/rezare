import cx from 'classnames';
import React from 'react';
import { useHistory } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import RezareLogo from '../assets/RezareLogo.png';

const useStyles = makeStyles((theme) => ({
  root: {
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    maxWidth: 600,
    padding: theme.spacing(2),
    width: '100%',
  },
  buttonWrapper: {
    marginTop: theme.spacing(2),
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'column',
  },
  logo: {
    backgroundImage: `url("${RezareLogo}")`,
    backgroundRepeat: 'none',
    backgroundSize: 'cover',
    width: 300,
    height: 150,
    marginBottom: theme.spacing(2),
    // Logo feels heavy to one side
    marginLeft: -8,
  },
  button: {
    padding: theme.spacing(0.5, 3),
    fontSize: '18pt',
  },
}));

const Login: React.FC = () => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <div className={classes.center}>
          <Typography gutterBottom>Practical Test App</Typography>
          <Typography gutterBottom>
            by <strong>Timothy Miller</strong>
          </Typography>
          <Typography gutterBottom>for</Typography>
        </div>
        <div className={classes.center}>
          <div className={classes.logo} />
        </div>
        <div className={cx(classes.center, classes.buttonWrapper)}>
          <Button
            className={classes.button}
            color="secondary"
            variant="contained"
            onClick={() => history.push('/students')}
          >
            Enter
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Login;
