import React, { useState } from 'react';
import Fuse from 'fuse.js';

import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import TextField from '@material-ui/core/TextField';

import UserTable from './students/StudentTable';
import PageHeading from '../components/PageHeading';

import useStudents from '../hooks/useStudents';
import CenteredSpinner from '../components/CenteredSpinner';

import StudentDialog from './students/StudentDialog';

import { Student } from '../types';

export interface StudentDialogState {
  open: boolean;
  student: Student | null;
}

const initialDialogState = { open: false, student: null };

const Students: React.FC = () => {
  const [search, setSearch] = useState<string>('');
  const [dialogState, setDialogState] = useState<StudentDialogState>(initialDialogState);

  const { data: students, error, isLoading, refetch } = useStudents();

  // Client side fuzzy search using fuse.js
  const fuse = new Fuse(students, {
    keys: ['firstName', 'lastName', 'age', 'avgGrade'],
    isCaseSensitive: false,
    threshold: 0.2,
  });

  return (
    <>
      <PageHeading title="Students" description="View and search through the list of students" />

      <Card>
        <CardHeader
          title={
            <Box width={400} maxWidth="80%">
              <TextField
                fullWidth
                variant="outlined"
                label="Search"
                helperText="Search the list of students"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </Box>
          }
          action={
            <Button
              color="primary"
              variant="contained"
              onClick={() => setDialogState({ open: true, student: null })}
            >
              Add student
            </Button>
          }
          style={{ paddingBottom: 0 }}
        />
        <CardContent>
          {isLoading ? (
            <div style={{ height: 100 }}>
              <CenteredSpinner label="Loading" size={50} />
            </div>
          ) : error ? (
            <Alert
              severity="error"
              action={
                <Button variant="outlined" onClick={() => refetch()}>
                  Retry
                </Button>
              }
            >
              There was an error fetching the students
            </Alert>
          ) : (
            <UserTable
              students={search ? fuse.search(search).map((res) => res.item) : students}
              setDialogState={setDialogState}
            />
          )}
        </CardContent>
      </Card>

      <StudentDialog
        initialValue={dialogState.student}
        open={dialogState.open}
        onClose={() => setDialogState(initialDialogState)}
      />
    </>
  );
};

export default Students;
