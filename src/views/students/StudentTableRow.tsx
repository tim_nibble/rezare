import React from 'react';

import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { fade, makeStyles } from '@material-ui/core/styles';

import { Student } from '../../types';

const useStyles = makeStyles((theme) => ({
  row: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: fade(theme.palette.primary.main, 0.1),
    },
    transition: theme.transitions.create('background-color', { duration: 20 }),
  },
}));

interface Props {
  student: Student;
  onClick: (student: Student) => void;
}

const UserTableRow: React.FC<Props> = ({ student, onClick }) => {
  const classes = useStyles();

  return (
    <TableRow className={classes.row} onClick={() => onClick(student)}>
      {Object.entries(student).map(([key, value]) => {
        if (key === '_id') {
          return null;
        }
        return (
          <TableCell key={key} width='25%'>
            {value}
          </TableCell>
        );
      })}
    </TableRow>
  );
};

export default UserTableRow;
