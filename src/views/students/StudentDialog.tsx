import React, { useEffect, useState } from 'react';
import { useQueryClient } from 'react-query';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { darken, fade, makeStyles, Theme } from '@material-ui/core/styles';

import { createStudent, updateStudent } from '../../actions/students';

import ResponsiveDialog from '../../components/ResponsiveDialog';

import { Student, getGradeInfo } from '../../types';

import { convertZodErrors } from '../../lib';

const useStyles = makeStyles<Theme, { color: string }>((theme) => ({
  root: {
    color: ({ color }) => darken(color, 0.4),
    backgroundColor: 'rgba(255,255,255,0.6)',
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(0.25, 0.5),
    marginLeft: -theme.spacing(0.25),
  },
  notchedOutline: {
    borderColor: ({ color }) => color,
    borderWidth: 2,
    backgroundColor: ({ color }) => fade(color, 0.2),
  },
}));

interface Props {
  initialValue: Student | null;
  open: boolean;
  onClose: () => void;
}

const StudentDialog: React.FC<Props> = ({ initialValue, open, onClose }) => {
  const qclient = useQueryClient();
  const [errors, setErrors] = useState<Record<string, string>>({});
  const [student, setStudent] = useState(
    initialValue || {
      _id: null,
      firstName: '',
      lastName: '',
      age: 0,
      avgGrade: 100,
    },
  );

  const gradeInfo = getGradeInfo(student.avgGrade);
  const classes = useStyles({ color: gradeInfo.color });

  const handleChange = (key: keyof Student, value: string | number) => {
    setStudent((prev) => ({
      ...prev,
      [key]: value,
    }));
  };

  const handleSave = () => {
    const crudFunction = student._id ? updateStudent : createStudent;
    // @ts-ignore student._id is checked above
    crudFunction(student)
      .then((res) => {
        qclient.refetchQueries('students');
        onClose();
      })
      .catch((err) => {
        if (err.response.data) {
          setErrors(convertZodErrors(err.response.data.fieldErrors));
        }
      });
  };

  useEffect(() => {
    if (initialValue) {
      setStudent(initialValue);
    }
  }, [initialValue]);

  const handleBlur = () => {
    setErrors({});
  };

  return (
    <ResponsiveDialog open={open} title="Student details" onClose={onClose}>
      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography gutterBottom>
              This student has <strong>{gradeInfo.label}</strong> grades
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              error={!!errors['firstName']}
              helperText={errors['firstName'] || ''}
              variant="outlined"
              value={student.firstName}
              label="First name"
              onBlur={handleBlur}
              onChange={(e) => handleChange('firstName', e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              error={!!errors['lastName']}
              helperText={errors['lastName'] || ''}
              variant="outlined"
              value={student.lastName}
              label="Last name"
              onBlur={handleBlur}
              onChange={(e) => handleChange('lastName', e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              error={!!errors['age']}
              helperText={errors['age'] || ''}
              variant="outlined"
              value={student.age}
              label="Age"
              type="number"
              onBlur={handleBlur}
              onChange={(e) =>
                handleChange('age', e.target.value === '' ? '' : Number.parseInt(e.target.value))
              }
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
              }}
              InputLabelProps={{
                classes: {
                  root: classes.root,
                },
              }}
              error={!!errors['avgGrade']}
              helperText={errors['avgGrade'] || ''}
              fullWidth
              variant="outlined"
              value={student.avgGrade}
              label="Average grade"
              type="number"
              onBlur={handleBlur}
              onChange={(e) =>
                handleChange(
                  'avgGrade',
                  e.target.value === '' ? '' : Number.parseInt(e.target.value),
                )
              }
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={onClose}>
          Close
        </Button>
        <Button color="primary" variant="contained" onClick={handleSave}>
          {student?._id ? 'Update' : 'Save'}
        </Button>
      </DialogActions>
    </ResponsiveDialog>
  );
};

export default StudentDialog;
