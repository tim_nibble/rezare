const express = require('express');
const z = require('zod');
const { v4: uuid } = require('uuid');

const students = require('../assets/students.json');

const router = express.Router();

let studentData = students;

// Simple GET to fetch the student data
router.get('/', (req, res, next) => {
  res.send({ success: true, data: studentData });
});

// Simple POST that pushes a new student
router.post('/', (req, res, next) => {
  const student = z
    .object({
      firstName: z.string().nonempty(),
      lastName: z.string().nonempty(),
      age: z.number().min(0),
      avgGrade: z.number().min(0).max(100),
    })
    .parse(req.body);

  const newStudent = {
    _id: uuid(),
    ...student,
  };

  studentData.unshift(newStudent);
  res.send({
    success: true,
    data: newStudent,
  });
});

router.put('/', (req, res, next) => {
  const student = z
    .object({
      _id: z.string().nonempty(),
      firstName: z.string().nonempty(),
      lastName: z.string().nonempty(),
      age: z.number().min(0),
      avgGrade: z.number().min(0).max(100),
    })
    .parse(req.body);

  const index = students.findIndex((s) => s._id === student._id);

  if (index === -1) {
    throw new Error(`Student id: ${student._id} does not exist.`);
  }

  students.splice(index, 1, student);

  res.send({
    success: true,
    data: student,
  });
});

module.exports = router;
