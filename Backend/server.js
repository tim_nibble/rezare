const express = require('express');
const bodyParser = require('body-parser');

const studentRoutes = require('./routes/students');

const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', '*');

  next();
});

app.use(bodyParser.json());

app.use('/api/students', studentRoutes, (err, req, res, next) => {
  res.status(500).send(err.flatten());
});

app.listen(5000);
